#include <cstdint>
#include <algorithm>
#include <string>
#include <cassert>

using namespace std;

class trie {
    static constexpr size_t R = 256;
    struct node {
        node* next[R];
        int val;
        node() { 
            fill_n(next, R, nullptr);
            val = 0;
        }
    };

    node* header = nullptr;

public:
    ~trie() { destory(); }

    node* get(const string& s) {
        if (s.empty()) return nullptr;
        node* y = header; 
        for (auto c: s) {
            if (!y) return nullptr;
            y = y->next[c];
        }
        return y && y->val ? y : nullptr;
    }

    void put(const string& s, int v = 1) {
        if (s.empty()) return ;

        node* y = header; 
        node** x = &header;

        auto make_xy = [&] {
            if (!y) *x = y = new node;
        };

        for (auto c: s) {
            make_xy();
            x = &(y->next[c]);
            y = y->next[c];
        }
        make_xy();
        (*x)->val = v;
    }

    void destory() {
        destory(header);
        header = nullptr;
    };

    void destory(node* x) {
        if (!x) return ;
        for (auto& y: x->next) {
            destory(y);
        }
        delete x;
    };
};


int main() {
    trie t;
    t.put("hello", 1);
    t.put("hello1", 2);
    assert(t.get("a") == nullptr);
    assert(t.get("he") == nullptr);
    assert(t.get("hello")->val == 1);
    assert(t.get("hello1")->val == 2);

    t.destory();
    assert(t.get("hello") == nullptr);
    t.put("hello", 3);
    t.put("hello1", 4);
    assert(t.get("a") == nullptr);
    assert(t.get("he") == nullptr);
    assert(t.get("hello")->val == 3);
    assert(t.get("hello1")->val == 4);
};
