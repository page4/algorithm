#include <string>
#include <vector>
#include <cassert>
#include <cstdio>
#include <iostream>
#include <algorithm>
#include <iterator>

std::vector<int> get_next(std::string s) {
    std::vector<int> next(s.size() + 1, 0);
    next[0] = -1;
    int i = 0, j = -1; // next[0] = -1

    // if s[i] matches with s[j], we want to match s[i + 1] witch s[j + 1],
    //      that is, next[i + 1] = j + 1;
    // if j equals -1, no charactor matches with s[i]. try match s[i + 1] with s[0]
    //      next[i + 1] = 0 = j + 1;
    while (i < s.size()) {
        if (j == -1 || s[i] == s[j]) {
            ++i;
            ++j;
            next[i] = j;
        } else {
            j = next[j];
        }
    }
    std::copy(next.begin(), next.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
    return next;
}

int kmp(std::string s, std::string t) {
    auto next = get_next(t);
    int i = 0, j = 0;
    // 
    while (i < s.size() && j < int(t.size())) {
        if (j == -1 || s[i] == t[j]) {
            ++i;
            ++j;
        } else {
            j = next[j];
        }
    }
    printf("maybe %d %d\n",  i,  j);
    if (j >= t.size()) return i - j;
    return -1;
};

int main() {
    assert(kmp("aaa", "a") == 0);
    assert(kmp("abc", "c") == 2);
    assert(kmp("abcbbca", "bca") == 4);
}
